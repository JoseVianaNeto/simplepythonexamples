#!/usr/bin/python3.2

import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-e", "--essid", help="Target ESSID")
parser.add_argument("-i", help="Interface")
parser.add_argument("-w", help="Name of file to write to")
args = parser.parse_args()

subprocess.call(["airmon-ng", "start", "wlan0"])
subprocess.call(["airodump-ng","--essid", args.essid,"-w", args.w, args.i])
