#!/usr/local/bin/python3.5

contador1 = 1
contador2 = 0

while contador1<1000:

	contador1= contador1+1	
	contador2=contador1-1
	primo = True
	
	while contador2>1:

		if (contador1%contador2 == 0) and primo:
	
			primo = False
			break	
		contador2 = contador2-1

	if primo:

		print(contador1, "é primo")

	else:

		print(contador1, "não é primo")	
