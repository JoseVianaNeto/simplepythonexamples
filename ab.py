#!/usr/local/bin/python3.5

import argparse

parse = argparse.ArgumentParser()

parse.add_argument("-y1", help="y1 in a y sequence")
parse.add_argument("-y2", help="y2 in a y sequence")
parse.add_argument("-x1", help="x1 in a x sequence")
parse.add_argument("-x2", help="x2 in a x sequence")

args = parse.parse_args()

class Ab(object):

	def __init__(self, y1,y2,x1,x2):
		
		self.a = self.setA(int(y1),int(y2),int(x1),int(x2))
		self.b = self.setB(self.a,int(x1),int(y1))

	def setB(self,a,x1,y1):

		b=y1-(a*x1)

		return b


	def setA(self,y1,y2,x1,x2):

		a = (y2-y1)/(x2-x1)

		return a

	def getA(self):

		return self.a
	
	def getB(self):

		return self.b

res = Ab(args.y1,args.y2,args.x1,args.x2)

print("y=",str(res.getA()),"x+",str(res.getB()), sep="")
